### Welcome to Greater Life Ministries and Church
![logo](http://learning.glifechurch.com/sites/default/files/2017GlifeLogo-trans%20copy_0.png)

# Managing your finances module #
___

 [Learn more... click here ](https://gitlab.com/glifechurch/how-to-manage-finances/wikis/home)

### What is a learning module? ###


Learning modules are [Drupal 7](http://drupal.org) custom modules, content and 3rd party modules. 

As dependencies these modules combined create learning modules that become available to life long learnings who use the site for spiritual and personal development. 

For more specific details about this module see this projects.


![Learning module diagram](/uploads/efe938a5dd3f1948802a59518d44d7a1/learning-mod-description_2_.jpg)


### Module Description: ###

This learning module on "Managing Your Finances" is not about proving if you should or should not pay tithes, 
however; it does solve a problem and answers a question about proving God through financial management. 
Tithing or giving to others while on a fixed budget or when spending is out of control can be challenging, it can also 
be challenging if leadership places high emphasis on giving. "My people perish for the lack of knowledge!" Giving was 
introduced by God as a way answering problems associated with sin in the flesh. If done properly will loosening the 
connection between selfishness and selflessness, but the weight of that 
burden (on the flesh) changed Gods meaning of giving and is the problem this lesson will address.
Phrases such as "when you see your brother in need; care for widows; care for one another" are biblical and 
your ability to participate reflects a person after God's own heart. In this learning module you will learn the steps 
to changing your financial situation so that you can enjoy the benefits of living in peace and giving.